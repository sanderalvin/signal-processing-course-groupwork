# -*- coding: utf-8 -*-
import numpy as np
from scipy.fftpack import fft, fftfreq, fftshift
from scipy.io.wavfile import read
import matplotlib.pyplot as plt




# cuts off zero values from numpy array
def cut_zero_values(array):
    start_index = 0
    for elem in array:
        if elem == 0:
            start_index += 1
        else:
            break
    return array[start_index:]


# choose a subsection from array
def take_piece_of_sound(array):
    maximum = max(array)
    compare = 0.85 * maximum
    start = 0
    end = -1
    for i in range(len(array)):
        if start == 0 and array[i] > compare:
            start = i
        elif array[i] > compare:
            end = i
    return array[start:end]


# from time domain to frequency domain
def freq_spectrum(input_data):
    audio = input_data[1]
    audio = take_piece_of_sound(audio)
    # number of signal points
    N = len(audio)
    # sample spacing
    T = 1.0 / input_data[0]
    # time domain x-axis
    # x = np.linspace(0.0, N * T, N)
    y = audio
    # applying FFT to y
    yf = fft(y)
    # freq domain x axis
    xf = fftfreq(N, T)
    xf = fftshift(xf)
    yplot = fftshift(yf)
    # returning only positive freq-s because function
    # is symmetric with respect to y-axis
    return xf[N / 2:], 2.0 / N * np.abs(yplot[N / 2:]), N / 2, T


# get fundamental freq from spectrum
def fundamental_freq(spectra):
    # index pointing to maximum element
    max_spot = spectra.argmax()
    maximum = max(spectra)
    low_1 = None
    low_2 = None
    percent = 0.02
    compare_value = maximum * percent
    # find neighbouring maximas
    for i in range(max_spot, len(spectra)):
        if not low_1 and spectra[i] < compare_value:
            low_1 = i
        elif low_1 and not low_2 and spectra[i] > compare_value * 4:
            low_2 = -1
        elif low_2 and spectra[i] < compare_value:
            low_2 = i
            break
    max_spot_2 = low_1 + spectra[low_1:low_2].argmax()
    return max_spot_2 - max_spot


def harmonic_array(freq_spectrum):
    fund_freq = fundamental_freq(freq_spectrum)
    #print "fund freq", fund_freq
    interval = int(fund_freq * 0.25)
    harmonics = []
    how_many = 5 #NB!
    position = 0 + fund_freq
    i = 0
    try:
        while i < how_many:
            # arvutan siin intergreerides igale harmoonikule vastava energia
            added = sum(freq_spectrum[(position - interval):(position + interval)]**2)
            harmonics.append(added)
            position += fund_freq
            i += 1
    except IndexError:
        print "freq_spectrum array does not contain enough elements when finding harmonics, "
    except ValueError:
        print "freq_spectrum array does not contain enough elements when finding harmonics, "
    # normeerime esimese viie harmooniku energiate summa järgi
    if harmonics[0]>0:
        harmonics=harmonics/sum(harmonics)
    print "prindin harmoonikuid", harmonics, harmonics.sum()
    return harmonics


"""
# get first 10 harmonics from freq spectrum
def harmonic_array(freq_spectrum, interval_percent=0.48):
    fund_freq = fundamental_freq(freq_spectrum)
    #print "fund freq", fund_freq
    interval = int(fund_freq * interval_percent)
    harmonics = []
    how_many = 10
    position = 0 + fund_freq
    i = 0
    try:
        while i < how_many:
            added = max(freq_spectrum[(position - interval):(position + interval)])
            harmonics.append(added)
            position += fund_freq
            i += 1
    except IndexError:
        print "freq_spectrum array does not contain enough elements when finding harmonics, "
    except ValueError:
        print "freq_spectrum array does not contain enough elements when finding harmonics, "
    maximum = max(harmonics)
    # normalize so that that maximum element is 1
    return harmonics / maximum
"""

# get the sound name from file location
# input "./vokaalid/wavid/õ.wav"  returns "õ"
def get_sound_name(location):
    slash_list = location.split("/")
    print slash_list
    return slash_list[-1].split(".")[0]


# plot frequency spectre
def show_spectre(file_location):
    import functions
    input_data = read(file_location)
    xf, yplot, N, T = functions.freq_spectrum(input_data)
    plt.plot(xf, yplot, '-r')
    plt.grid()
    # label the axes
    plt.ylabel("Magnitude (dB)")
    plt.xlabel("Frequency Bin")
    # set the title
    plt.title(functions.get_sound_name(file_location) + " Spectrum")
    #print "maksimum", yplot.argmax(), max(yplot)
    fundamental_freq = functions.fundamental_freq(yplot)
    #print "fundamental freq", fundamental_freq
    step = 0.5 / (T * N)
    #print "vahesamm on", step
    #print "vahesamm on teist moodi samas", xf[1] - xf[0]


    marker = 0
    i = 0
    interval = fundamental_freq * step * 0.48
    while i < 11:
        marker += fundamental_freq * step
        plt.axvline(marker)
        #plt.axvline(marker + interval, color='#66ff66')
        #plt.axvline(marker - interval, color='#66ff66')
        i += 1
    #print "ja harmoonikud on:", harmonic_array(yplot)
    plt.show()

# this function shows signal in time domain
def show_time_representation(file_location):
    # read audio samples
    input_data = read(file_location)
    # this is sampling rate
    audio_info = input_data[0]
    audio = input_data[1]
    #plt.plot(functions.take_piece_of_sound(audio))
    plt.plot(audio)
    # label the axes
    plt.ylabel("Amplitude")
    plt.xlabel("Time (samples)")
    plt.title(get_sound_name(file_location))
    # display the plot
    plt.show()



# return .wav file from file location
def wav_file(file_location):
    from scipy.io.wavfile import read
    return  read(file_location)



# return harmonics from file location
def file_location_to_harmonics(file_location):
    return harmonic_array(freq_spectrum(wav_file(file_location))[1])