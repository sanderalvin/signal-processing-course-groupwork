# -*- coding: utf-8 -*-
from functions import *
from os import listdir
from os.path import isfile, join
import math


class Vowel:
    def __init__(self, **kwargs):
        self.name = kwargs.get("name")
        self.harmonics = kwargs.get('harmonics')
        file = kwargs.get('file_location')
        if file:
            self.name = get_sound_name(file)
            self.harmonics = file_location_to_harmonics(file)

    def to_string(self):
        print "Name : ", self.name, "\n ", ", Harmonics: ", self.harmonics

    # # compare Vowel with another, the smaller the score, the more similar two vowels are
    # def compare_with_other(self, other):
    #     score=0.0
    #     sum=abs(self.harmonics.sum())
    #     for i in range(len(self.harmonics)):
    #         score+=abs(self.harmonics[i]-other.harmonics[i]) *abs(self.harmonics[i])/sum
    #     return score

    # def compare_with_other(self, other):
    #     score=0.0
    #     sum=abs(self.harmonics.sum())
    #     for i in range(len(self.harmonics)):
    #         score+=abs(self.harmonics[i]-other.harmonics[i])**2
    #     return score


    # mihkel kree function
    def compare_with_other(self, other):
        score = 0.0
        for i in range(len(self.harmonics)):
            if (self.harmonics[i] == 0 or other.harmonics[i] == 0):
                continue
            # arvutame suhteliste erinevuste kümnendlogaritmide summa
            score += abs(math.log(self.harmonics[i] / other.harmonics[i], 10))
        return score





    # returns list of matching vowels and scores (sorted by score in descending order)
    def best_fit(self, vowel_list):
        array = []
        for elem in vowel_list:
            score = self.compare_with_other(elem)
            array.append((elem, score,))
        array = sorted(array, key=lambda x: x[1])
        # print
        # for elem in vowel_list:
        #     print "võrdlemine: " , elem.name,  self.harmonics, "\n", elem.harmonics
        return array


# create list with vowels as members, input is directory conainting .wav files
def vowel_list(folder="./paremad_vokaalid"):
    vowels = []
    onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
    print onlyfiles
    for elem in onlyfiles:
        u = unicode(elem, "utf-8")
        file_location = folder + "/" + u
        input_data = wav_file(file_location)
        freq = freq_spectrum(input_data)[1]
        name = get_sound_name(file_location)
        harmonics = harmonic_array(freq)
        vowel = Vowel(name=name, harmonics=harmonics)
        vowels.append(vowel)
    return vowels
