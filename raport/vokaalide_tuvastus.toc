\select@language {estonian}
\contentsline {chapter}{Joonised}{3}{chapter*.2}
\contentsline {chapter}{\numberline {1}Sissejuhatus}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}\IeC {\"U}lesande p\IeC {\"u}stitus}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Faili sisselugemine}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Vokaali ajaesitus}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Ajaesitusest sagedusesitusse}{5}{section.1.4}
\contentsline {chapter}{\numberline {2}Vokaalide v\IeC {\~o}rdlus}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Kuidas vokaale v\IeC {\~o}rrelda?}{8}{section.2.1}
\contentsline {section}{\numberline {2.2}P\IeC {\~o}hisageduse saamise algoritm}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Sagedusspektrist harmoonikumite saamise algoritm}{9}{section.2.3}
\contentsline {section}{\numberline {2.4}Erinevate vokaalide v\IeC {\~o}rdlemise algoritm}{10}{section.2.4}
\contentsline {chapter}{\numberline {3}Tulemused}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Tulemused esimesel meetodil}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Tulemused Mihkel Kree muudatustega}{16}{section.3.2}
\contentsline {chapter}{\numberline {4}Kokkuv\IeC {\~o}te}{21}{chapter.4}
