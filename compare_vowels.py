# -*- coding: utf-8 -*-

import vowel
import functions
# to see vocal spectre uncomment line below
#functions.show_spectre(u"./paremad_vokaalid/i.wav")
#functions.show_spectre(u"./test_vokaalid/alvin_i_madal.wav")

# exit()
# vocal etalons
vowels=vowel.vowel_list("./paremad_vokaalid")

# vocals which are gonna be compared against etalons
to_be_tested=vowel.vowel_list("./test_vokaalid")

for testitav in to_be_tested:
    print
    print "Testing vocal: ", testitav.name
    for elem in testitav.best_fit(vowels):
        print elem[0].name, "\t", elem[1]