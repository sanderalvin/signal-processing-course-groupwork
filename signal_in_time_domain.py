# -*- coding: utf-8 -*-

# this script shows signal in time domain


from scipy.io.wavfile import read
import matplotlib.pyplot as plt
import functions

# read audio samples
#file_location=u"./demo.wav"
file_location=u"./paremad_vokaalid/ä.wav"
input_data = read(file_location)
# this is sampling rate
audio_info = input_data[0]
audio = input_data[1]
print "sampling rate", audio_info


#plt.plot(functions.take_piece_of_sound(audio))
plt.plot((audio))
# label the axes
plt.ylabel("Amplitude")
plt.xlabel("Time (samples)")
plt.title(functions.get_sound_name(file_location))
# display the plot
plt.show()

