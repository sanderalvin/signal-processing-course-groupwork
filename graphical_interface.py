# -*- coding: utf-8 -*-
import ttk
from Tkinter import *

from record import record_to_file
from vowel import  Vowel, vowel_list
import functions




# näitab programmis käimasolevat protsessijada
def massiiviTeavitaja(massiiv):
    text.delete(1.0, END)
    for jupp in massiiv:
        text.insert(INSERT, (jupp[0].name).encode('utf-8')+"\t"+str(jupp[1]) + "\n")



# GUI
raam = Tk()
raam.title("Vokaalituvastus")
#raam.resizable(False, False)
raam.geometry("400x200")



file_location='demo.wav'


def create_vowel():
    global file_location
    vowel = Vowel(file_location=file_location)
    result=vowel.best_fit(vowels)
    massiiviTeavitaja(result)


vowels=vowel_list("./paremad_vokaalid")
# this button records for 1 second after pressing
record_button = ttk.Button(raam, text="Salvesta", command = lambda : record_to_file(file_location))
record_button.place(x=10, y=10, height=25, width=120)

create_vowel_button = ttk.Button(raam, text="Vokaalituvastus", command = lambda : create_vowel())
create_vowel_button.place(x=10, y=40,height=25, width=120)

specter_button = ttk.Button(raam, text="Spekter", command = lambda : functions.show_spectre(file_location))
specter_button.place(x=10, y=70, height=25, width=120)

RR_nupp = ttk.Button(raam, text="Ajaesitus", command = lambda : functions.show_time_representation(file_location))
RR_nupp.place(x=10, y=110,height=25, width=120)


text = Text(raam, width=25, height=9)
text.place(x=160, y=30)

raam.mainloop()